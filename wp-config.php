<?php
/** Enable W3 Total Cache Edge Mode */
define('W3TC_EDGE_MODE', true); // Added by W3 Total Cache


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hotel-javor');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!FEiYvu7h.SzYU>^y}_A3e4<(=M/<U0n^o+S4S91dKZ:yGELK~>iF+AarRyL{^=X');
define('SECURE_AUTH_KEY',  '@[|RM-ps5K(DV Fo!|,Z:-ckhr=/i|eEun!^yH^Pr<.VA4n;hiWo4F.T(ahi~OV?');
define('LOGGED_IN_KEY',    '-x//tV=$:G[O34F@dNhiKi4{|1:j]|phb0Y!f?_aQt9v|__b{Cx^p|K^1.bPIlvK');
define('NONCE_KEY',        '!rJ$Hg&L/5yN3Ekr+`)X#ZrxuC-|o9)EyM+%B3y-H+/5&#OF{5_RK#V#|lK*K/#(');
define('AUTH_SALT',        'b!de2FB%qigu2%>7:PpNGbF(}W0,CRD|ER .65~+j`3#VFAGc6Y91+Jg,$0!L^,$');
define('SECURE_AUTH_SALT', 'jxQt*(Z)by%<24E=[0|O+8U(|jkPxTUxT]qfDO1EX_,y^B;Z3)|y2Iv+&^v|,m{2');
define('LOGGED_IN_SALT',   '3nP+<J|hwJ_mEE*C|DtOy|%k=2V:NM|F|a}}B$]v%}D1zoEH.%^L#h$npL*T^x.m');
define('NONCE_SALT',       'a8k<>@FP73`I[<aP#1nacsD33L6-C]M) $=5(IG}fQ 8;aF8_;^Dp}kGq`Bns`)M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/**
 * Turn on/off maintenance mode.
 *
 */
define('IN_MAINTENANCE', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

