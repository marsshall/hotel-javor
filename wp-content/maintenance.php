<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Stránka nedostupná</title>
    <style type="text/css">
      body { text-align: center; padding: 150px; }
      h1 { font-size: 50px; }
      body { font: 20px Helvetica, sans-serif; color: #333; }
      #article { display: block; text-align: left; width: 650px; margin: 0 auto; }
      a { color: #dc8100; text-decoration: none; }
      a:hover { color: #333; text-decoration: none; }
    </style>

</head>
<body>
    <div id="article">
    <h1>Pracujeme na tom...</h1>
    <div>
        <p>Omlouváme se, ale www stránka je dočasně nedostupná. Věřím, že brzy tuto zprávu neuvidíte. Děkujeme za pochopení.</p>
        <p>&mdash; Horský hotel Javor</p>
    </div>
    </div>
</body>
</html>