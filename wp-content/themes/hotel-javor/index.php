<?php
get_header(); 

$col= '';
$children = get_pages('child_of='.$post->ID);
if( count( $children ) != 0 || $post->post_parent) {
    $col = 'has-sidebar';
}
?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<main role="main">
    <?php if ( has_post_thumbnail() ) { 
      $has_thumb = 'has-thumbnail';
      } 
      else {
        $has_thumb = '';
      }
    ?>
    <section class="page-head <?php echo $has_thumb; ?>">
         <?php if ( has_post_thumbnail() ) { 
          $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
          ?>
           <div class="ph-title">
             <div class="inn" style="background: url('<?php echo $thumbnail_src[0]; ?>') no-repeat center;background-size:cover;">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                       <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
             </div>
           </div>
          <?php } else { ?>
            <div class="ph-title">
               <div class="inn">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                     <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
               </div>
            </div>
          <?php } ?>
          <div class="ph-breadcrumbs">
           <div class="inn">
              <div class="container">
                <?php the_breadcrumb(); ?>
              </div>
           </div>
          </div>
    </section>
    <section class="page-content">
        <div class="container">
           <?php if(count( $children ) != 0 || $post->post_parent) : ?>
             <div class="page-row">
               <div class="sidebar">
                 <?php
                  if($post->post_parent)
                      $children = wp_list_pages('title_li=&child_of='.$post->post_parent.'&echo=0&sort_column=menu_order'); 
                  else
                      $children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0&sort_column=menu_order');
                  if ($children) {
                ?>
                <ul class="side-nav">
                    <?php echo $children; ?>
                </ul>
                <?php } ?>
               </div>
               <div class="content <?php echo $col; ?>">
                 <div class="entry editor-output">
                   <?php the_content(); ?>
                 </div>
               </div>
               <?php dynamic_sidebar("socialize"); ?>
             <?php else : ?>
                <div class="content">
                  <div class="entry editor-output">
                    <?php the_content(); ?>
                  </div>
                </div>
                <?php dynamic_sidebar("socialize"); ?>
           </div>
           <?php endif; ?>
        </div>
    </section>
</main>
<?php endwhile; endif; ?>

<?php get_footer(); ?>