<?php

/**
 * Themes support stuff
 */
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

// Registering wp_nav
register_nav_menus( array(
    'header-menu'   => __('Header menu'),
    'footer-menu'    => __('Footer menu'),
    'featured-menu'    => __('Featured menu')
));


/**
 * Disabling admin bar 
 */
add_filter('show_admin_bar', '__return_false'); 

/**
 * Remove unnecessary wp nav items
 */
function remove_menus(){
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'edit-comments.php' );          //Comments
}
add_action( 'admin_menu', 'remove_menus' );


/**
 * HTML title
 */
function html_title()
{
    if ( is_home() )
    {
            bloginfo( 'name' );
            echo ' | ';
            bloginfo( 'description' );
    }
    else
    {
            bloginfo( 'name' );
            wp_title( '|', true, 'left' );
           
    }
}

/**
 * Custom read more link
 */
add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
return '<a class="view-more" href="' . get_permalink() . '">Číst více</a>';
}


/**
 * Multi post Thumbnails - plugin needed
 */
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Alternatívni náhledový obrázek',
            'id' => 'secondary-image',
            'post_type' => 'hjrooms'
        )
    );
}


/**
 * Remove junks from head
 */
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator'); //removes WP Version # for security
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );


/**
 * Create HTML list of nav menu items.
 * Replacement for the native Walker, using the description.
 */
class Description_Walker extends Walker_Nav_Menu {

    function start_el(&$output, $item, $depth, $args)
    {
        $classes     = empty ( $item->classes ) ? array () : (array) $item->classes;

        $class_names = join(
            ' '
        ,   apply_filters(
                'nav_menu_css_class'
            ,   array_filter( $classes ), $item
            )
        );

        ! empty ( $class_names )
            and $class_names = ' class="'. esc_attr( $class_names ) . '"';

        // Build default menu items
        $output .= "<li id='menu-item-$item->ID' $class_names>";

        $attributes  = '';

        ! empty( $item->attr_title )
            and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
        ! empty( $item->target )
            and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
        ! empty( $item->xfn )
            and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
        ! empty( $item->url )
            and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';

        // Build the description (you may need to change the depth to 0, 1, or 2)
        $description = ( ! empty ( $item->description ) and 0 == $depth )
            ? '<small class="nav-desc">'.  $item->description . '</small>' : '';

        $title = apply_filters( 'the_title', $item->title, $item->ID );

        $item_output = $args->before
            . "<a $attributes>"
            . $args->link_before
            . $title
            . $description
            . '</a> '
            . $args->link_after
            . $args->after;

        // Since $output is called by reference we don't need to return anything.
        $output .= apply_filters(
            'walker_nav_menu_start_el'
        ,   $item_output
        ,   $item
        ,   $depth
        ,   $args
        );
    }
}

// Allow HTML tags in descriptions in WordPress Menu
remove_filter( 'nav_menu_description', 'strip_tags' );
add_filter( 'wp_setup_nav_menu_item', 'cus_wp_setup_nav_menu_item' );
function cus_wp_setup_nav_menu_item( $menu_item ) {
     $menu_item->description = apply_filters( 'nav_menu_description', $menu_item->post_content );
     return $menu_item;
}


/**
 * WP Enqueue scripts
 */ 
// Deregister and register new jquery
wp_deregister_script('jquery');
wp_register_script('jquery', get_template_directory_uri() .'/assets/js/jquery-2.1.3.min.js', '2.1.3', 1);

if( ! function_exists('web_scripts') ){
// adding scripts
    add_action('wp_enqueue_scripts', 'web_scripts');
    function web_scripts() {
    // Javascripts
         wp_enqueue_script('jquery-visible',        get_template_directory_uri() . '/assets/js/jquery.visible.min.js', array('jquery'), null, false);
        wp_enqueue_script('bootstrap-js',   get_template_directory_uri() . '/assets/js/vendors/bootstrap/bootstrap.js', array('jquery'), null, false);
        wp_enqueue_script('mansonry-js',        get_template_directory_uri() . '/assets/js/masonry.pkgd.min.js', array(), null, false);
        wp_enqueue_script('gallery-js',        get_template_directory_uri() . '/assets/plugins/gallery/js/jquery.lightbox.min.js', array(), null, false);
        wp_enqueue_script('tabSlideOut-js',        get_template_directory_uri() . '/assets/js/jquery.tabSlideOut.v1.3.js', array(), null, false);
        wp_enqueue_script('main-js',        get_template_directory_uri() . '/assets/js/application.js', array(), null, true);
        
    
    // Stylesheet   
        wp_enqueue_style('style',           get_template_directory_uri() . '/style.css', array(), null, false);
        wp_enqueue_style('gallery-style',           get_template_directory_uri() . '/assets/plugins/gallery/css/jquery.lightbox.css', array(), null, false);
        wp_enqueue_style('editor-style', get_template_directory_uri() . '/assets/css/editor.css', array(), null, false);   
        wp_enqueue_style('extra',           get_template_directory_uri() . '/assets/css/extra.css', array(), null, false);
    }
}


/**
 * Change default login form to administration
 */ 
function my_login_style() { ?>
    <style type="text/css">
        body.login {
            position: relative;
            background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/main-bg.png)repeat;
        }
        body.login:after {
            content: '';
            position: absolute;
            top: 0;
            background: #048dc6;
            height: 4px;
            display: block;
            width: 100%;
        }
        body.login:before {
            content: '';
            position: absolute;
            bottom: 0;
            background: #8db93c;
            height: 4px;
            display: block;
            width: 100%;
        }
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-trans.png);
            padding-bottom: 0px;
            background-size: 192px 78px;
            width: 192px;
            height: 78px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_style' );
