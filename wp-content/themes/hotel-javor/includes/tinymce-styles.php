<?php

/**
 * Apply styles to the visual editor
 */ 
add_filter('mce_css', 'my_editor_style');
function my_editor_style($url) {
 
    if ( !empty($url) )
        $url .= ',';
 
    // Retrieves the plugin directory URL
    // Change the path here if using different directories
    $url .= get_template_directory_uri() . '/assets/css/editor.css';
 
    return $url;
}
 
/**
 * Add "Styles" drop-down
 */
add_filter( 'mce_buttons_2', 'my_editor_buttons' );
 
function my_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
 
/**
 * Add styles/classes to the "Styles" drop-down
 */
add_filter( 'tiny_mce_before_init', 'my_before_init' );
 
function my_before_init( $settings ) {
 
    $style_formats = array(
        array(
            'title' => 'Úvodní intro text',
            'selector' => 'p',
            'classes' => 'lead-text',
        ),
        array(
            'title' => 'Nečíslovaný seznam',
            'selector' => 'ul',
            'classes' => 'styled',
        ),
    );
 
    $settings['style_formats'] = json_encode( $style_formats );
 
    return $settings;
 
}

// Stop messing your code
function override_mce_options($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');
 
?>