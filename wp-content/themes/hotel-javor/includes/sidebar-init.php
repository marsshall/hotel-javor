<?php 
function hj_widgets_init() {
   // Reception & reservation contacts 
   // Location: header
   register_sidebar(array(
      'name'               => 'Kontakt na recepci a rezervaci',
      'id'                 => 'reception-contacts',
      'description'   => __( 'Kontakty jsou umístěny v horní části stránky v oblasti slider-u a hlavičky.'),
      'before_widget' => '<div class="reception-contacts">',
      'after_widget' => '</div>',
      'before_title' => '',
      'after_title' => '',
   ));

   // Newsletter subscribe
   // Location: footer
   register_sidebar(array(
      'name'               => 'Odebíraní novinek',
      'id'                 => 'newsletter-subscribe',
      'description'   => __( 'Odebírání novinek je umístěno v patičce webu.'),
      'before_widget' => '<div class="newsletter-subscribe">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>',
   ));

   // Footer links
   // Location: footer
   register_sidebar(array(
      'name'               => 'Odkazy v patičce',
      'id'                 => 'footer-links',
      'description'   => __( 'Odkazy jsou umístěny v patičce webu.'),
      'before_widget' => '<div class="footer-links">',
      'after_widget' => '</div>',
      'before_title' => '<h3>',
      'after_title' => '</h3>',
   ));

   // Footer copyright
   // Location: footer
   register_sidebar(array(
      'name'               => 'Copyright text',
      'id'                 => 'footer-copyright',
      'description'   => __( 'Copyright text je umístěny v patičce webu.'),
      'before_widget' => '<div class="copyright">',
      'after_widget' => '</div>',
      'before_title' => '',
      'after_title' => '',
   ));

   // Socialize
   // Location: At the bottom of content
   register_sidebar(array(
      'name'               => 'Sociálni média',
      'id'                 => 'socialize',
      'description'   => __( 'Sociálni média jsou umístnena pod hlavním obsahem.'),
      'before_widget' => '<div class="socialize">',
      'after_widget' => '</div>',
      'before_title' => '',
      'after_title' => '',
   ));

   // Video 
   // Location: At the bottom of home page
   register_sidebar(array(
      'name'               => 'Video stream',
      'id'                 => 'video-stream',
      'description'   => __( 'Sociálni média jsou umístnena pod hlavním obsahem.'),
      'before_widget' => '<div class="video-wrapper">',
      'after_widget' => '</div>',
      'before_title' => '',
      'after_title' => '',
   ));

}
/** Register sidebars by running hj_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'hj_widgets_init' );
?>