<?php 
add_action('wp_loaded', function() {
    global $pagenow;
    if(
       defined('IN_MAINTENANCE')
       && IN_MAINTENANCE
       && $pagenow !== 'wp-login.php'
       && ! is_user_logged_in()
    ) {
       header('HTTP/1.1 Service Unavailable', true, 503);
       header('Content-Type: text/html; charset=utf-8');
       if ( file_exists(WP_CONTENT_DIR . '/maintenance.php') ) {
          require_once( WP_CONTENT_DIR . '/maintenance.php' );
       }
       die();
    }
});

?>