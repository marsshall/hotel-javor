<?php

// Post type: Partners 
add_action('init', function(){

    $labels = array(
        'name'                  => __( 'Partneři'),
        'singular_name'         => __( 'Partner'),
        'menu_name'             => __( 'Partneři'),
        'all_items'             => __( 'Všechny partneři'),
        'add_new'               => __( 'Přidat nového'),
        'add_new_item'          => __( 'Přidat nového partnera'),
        'edit_item'             => __( 'Upravit partnera'),
        'new_item'              => __( 'Nový partnera'),
        'view_item'             => __( 'Zobrazit partnera'),
        'search_items'          => __( 'Vyhledávat partnera'),
        'not_found'             => __( 'No item found'),
        'not_found_in_trash'    => __( 'No item found in Trash')
        );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'menu_icon'             => get_template_directory_uri() . '/includes/images/icon_partners.png',
        'rewrite'               => true,
        'capability_type'       => 'page',
        'show_in_nav_menus'     => false,
        'supports'              => array('title','thumbnail') 
        );
    register_post_type('hjpartners', $args);
    flush_rewrite_rules();
});



// partners metaboxes
add_filter( 'cmb2_meta_boxes', 'hjpartners_metabox' );

function hjpartners_metabox( array $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_partner_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $meta_boxes['partner-options'] = array(
        'id'            => 'partner-options',
        'title'         => __( 'Nastavení partnera'),
        'object_types'  => array( 'hjpartners'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
        'fields'        => array(
            array(
                'name' => __( 'URL partnera'),
                'desc' => __( 'Zadejte adresu partnera.'),
                'id'   => $prefix . 'url',
                'type' => 'text_url',
                'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto'), // Array of allowed protocols
            )
        ),
    );

    // Add other metaboxes as needed

    return $meta_boxes;
}

// Adding custom columns
add_filter( 'manage_edit-hjpartners_columns', 'my_edit_hjpartners_columns' ) ;

function my_edit_hjpartners_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Název partnera' ),
        'thumb' => __( 'Obrázek partnera' ),
        'url' => __( 'Web partnera' )
    );

    return $columns;
}

add_action( 'manage_hjpartners_posts_custom_column', 'my_manage_hjpartners_columns', 10, 2 );

// Adding content to custom columns
function my_manage_hjpartners_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        /* If displaying the 'thumb' column. */
        case 'thumb' :
            /* Get the thumb for the post. */
            $thumb = get_the_post_thumbnail( $post_id, 'thumbnail');
            /* If no duration is found, output a default message. */
            if ( empty( $thumb ) )
                echo __( '-' );

            /* If there is a thumb, append thumnbail img */
            else
                echo $thumb;
            break;

        /* If displaying the url column. */
        case 'url' :

            /* Get the url for the post. */
            $partner_url = get_post_meta( $post_id, '_partner_url', true );

            /* If terms were found. */
            if ( empty( $partner_url ) ) {
                echo __( '-' );
            }

            /* If there is an url, display it */
            else {
                echo $partner_url;
            }
            break;

        /* Just break out of the switch statement for everything else. */
        default :
            break;
    }
}