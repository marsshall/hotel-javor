<?php

// Post type: Sliders 
add_action('init', function(){

    $labels = array(
        'name'                  => __( 'Slide'),
        'singular_name'         => __( 'Slide'),
        'menu_name'             => __( 'Slider'),
        'all_items'             => __( 'Všetky slide'),
        'add_new'               => __( 'Pridať nový'),
        'add_new_item'          => __( 'Pridať nový slide'),
        'edit_item'             => __( 'Upravit slide'),
        'new_item'              => __( 'Nový slide'),
        'view_item'             => __( 'Zobrazit slide'),
        'search_items'          => __( 'Vyhledávat slides'),
        'not_found'             => __( 'No item found'),
        'not_found_in_trash'    => __( 'No item found in Trash')
        );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'menu_icon'             => get_template_directory_uri() . '/includes/images/icon_slides.png',
        'rewrite'               => true,
        'capability_type'       => 'page',
        'show_in_nav_menus'     => false,
        'supports'              => array('title','thumbnail') 
        );
    register_post_type('hjslider', $args);
    flush_rewrite_rules();
});


// slider metaboxes
add_filter( 'cmb2_meta_boxes', 'hjslider_metabox' );

function hjslider_metabox( array $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_slider_';

    /**
     * Sample metabox to demonstrate each field type included
     */
    $meta_boxes['options'] = array(
        'id'            => 'options',
        'title'         => __( 'Nastavení slide-u'),
        'object_types'  => array( 'hjslider', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
        'fields'        => array(
            array(
                'name' => __( 'Text tlačítka'),
                'desc' => __( 'Zadejte text, který se má zobrazit v tlačítku.'),
                'id'   => $prefix . 'button_text',
                'type' => 'text',
                // 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
                // 'repeatable' => true,
            ),
            array(
                'name' => __( 'URL tlačítka'),
                'desc' => __( 'Zadejte adresu, na kterou mě tlačítko odkazovat.'),
                'id'   => $prefix . 'button_url',
                'type' => 'text_url',
                'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto'), // Array of allowed protocols
            )
        ),
    );

    // Add other metaboxes as needed

    return $meta_boxes;
}
