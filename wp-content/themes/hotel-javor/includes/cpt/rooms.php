<?php

/*-----------------------------------------------------------------------------------*/
/*  Define Custom Post Type
/*-----------------------------------------------------------------------------------*/

// Post type: Rooms 
add_action('init', function(){

    $labels = array(
        'name'                  => __( 'Typy pokojů'),
        'singular_name'         => __( 'Ubytování'),
        'menu_name'             => __( 'Ubytování'),
        'all_items'             => __( 'Všechny pokoje'),
        'add_new'               => __( 'Přidat nový'),
        'add_new_item'          => __( 'Přidat nový pokoj'),
        'edit_item'             => __( 'Upravit pokoj'),
        'new_item'              => __( 'Nový pokoj'),
        'view_item'             => __( 'Zobrazit pokoj'),
        'search_items'          => __( 'Vyhledávat pokoj'),
        'not_found'             => __( 'No item found'),
        'not_found_in_trash'    => __( 'No item found in Trash')
        );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'menu_icon'             => get_template_directory_uri() . '/includes/images/icon_rooms.png',
        'rewrite' => array('slug'=>'ubytovani'),
        'capability_type'       => 'page',
        'show_in_nav_menus'     => true,
        'supports'              => array('title','thumbnail') 
        );
    register_post_type('hjrooms', $args);
    flush_rewrite_rules();
});


// room metaboxes
add_filter( 'cmb2_meta_boxes', 'hjrooms_metaboxes' );

function hjrooms_metaboxes( array $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_room_';

    /**
     * Sample metaboxes
     */

    $meta_boxes['general_info'] = array(
        'id'            => 'general-info',
        'title'         => __( 'Všeobecní informace'),
        'object_types'  => array( 'hjrooms', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
        'fields'        => array(
            array(
                'name' => __( 'Všeobecní popis ubytování'),
                'desc' => __( 'napr. info o hotelu, okolí a pod.'),
                'id'   => $prefix . 'accommodation_info',
                'type' => 'wysiwyg',
                // 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
                // 'repeatable' => true,
            ),
            array(
                'name' => __( 'Pokyny k příjezdů'),
                'desc' => __( 'Ze seznamu vyberte stránku, která obsahuje pokyny k příjezdům (obsah stránky vytvoříte v sekci "Stránky").'),
                'id'   => $prefix . 'arrival_info',
                'type'    => 'select',
                'options' => cmb2_get_post_options( array( 'post_type' => 'page', 'numberposts' => -1 ) )
            )
        ),
    );

    $meta_boxes['room_description'] = array(
        'id'            => 'room-description',
        'title'         => __( 'Informace o pokoji'),
        'object_types'  => array( 'hjrooms', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
        'fields'        => array(
            array(
                'name' => 'Popis pokoje',
                'desc' => __( 'Zadejte krátky popis pokoje. Tento popis bude umístnen nad cenníkem pokoje.'),
                'id'   => $prefix . 'description',
                'type' => 'wysiwyg',
            ),
            array(
                'name' => __( 'Vybavení pokoje'),
                'desc' => __( 'Ze seznamu vyberte stránku, která obsahuje vybavení pokojů (obsah stránky vytvoříte v sekci "Stránky").'),
                'id'   => $prefix . 'accessories',
                'type'    => 'select',
                'options' => cmb2_get_post_options( array( 'post_type' => 'page', 'numberposts' => -1 ) )
            ),
            array(
                'name' => __( 'Hotelové služby'),
                'desc' => __( 'Ze seznamu vyberte stránku, která obsahuje hotelové služby (obsah stránky vytvoříte v sekci "Stránky").'),
                'id'   => $prefix . 'services',
                'type'    => 'select',
                'options' => cmb2_get_post_options( array( 'post_type' => 'page', 'numberposts' => -1 ) )
            )
        ),
    );

    $meta_boxes['room_info'] = array(
        'id'            => 'room-info',
        'title'         => __( 'Dodatoční informace o pokoji'),
        'object_types'  => array( 'hjrooms', ), // Post type
        'context'       => 'side',
        'priority'      => 'low',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
        'fields'        => array(
            array(
                'name' => 'Kapacita',
                'desc' => __( 'Zadejte počet lůžek na pokoji.'),
                'id'   => $prefix . 'capacity',
                'type' => 'text_small',
            ),
            array(
                'name' => 'Cena za pokoj se snídaní',
                'desc' => __( 'Zadejte cenu za pokoj. (cena = počet lůžek x cena za lůžko)'),
                'id'   => $prefix . 'price',
                'type' => 'text_small',
            ),
            array(
                'name' => 'Cena za pokoj s polopenzí',
                'desc' => __( 'Informace pod cenníkem.'),
                'id'   => $prefix . 'price_halfboard',
                'type' => 'textarea_small',
            ),
            array(
                'name' => 'Cena za dítě 4 - 9,99 let za noc se snídaní',
                'desc' => __( 'Informace pod cenníkem.'),
                'id'   => $prefix . 'price_children',
                'type' => 'textarea_small',
            )
        ),
    );

    $meta_boxes['room_photogallery'] = array(
            'id'            => 'room_photogallery',
            'title'         => __( 'Obrázková fotogalerie'),
            'object_types'  => array( 'hjrooms', ), // Post type
            'context'       => 'normal',
            'priority'      => 'low',
            'show_names'    => true, // Show field names on the left
            // 'cmb_styles' => false, // false to disable the CMB stylesheet
            // 'closed'     => true, // Keep the metabox closed by default
            'fields'        => array(
                array(
                    'name' => 'Fotky',
                    'desc' => 'Nahrajte fotky pokojů¯.',
                    'id' => $prefix . 'photogallery',
                    'type' => 'wysiwyg',
                
                )
            ),
        );

    // Add other metaboxes as needed

    return $meta_boxes;
}

// Get page options
function cmb2_get_post_options( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'page',
        'numberposts' => -1,
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }

    return $post_options;
}

// Wysiwyg filter
function get_wysiwyg_output( $meta_key, $post_id = 0 ) {
    global $wp_embed;

    $post_id = $post_id ? $post_id : get_the_id();

    $content = get_post_meta( 2, $meta_key, 1 );
    $content = $wp_embed->autoembed( $content );
    $content = $wp_embed->run_shortcode( $content );
    $content = do_shortcode( $content );
    $content = wpautop( $content );

    return $content;
}