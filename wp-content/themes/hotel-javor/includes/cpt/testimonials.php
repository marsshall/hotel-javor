<?php

/*-----------------------------------------------------------------------------------*/
/*  Define Custom Post Type
/*-----------------------------------------------------------------------------------*/

// Post type: Testimonials 
add_action('init', function(){

    $labels = array(
        'name'                  => __( 'Reference klientů'),
        'singular_name'         => __( 'Reference klientů'),
        'menu_name'             => __( 'Reference klientů'),
        'all_items'             => __( 'Všechny reference'),
        'add_new'               => __( 'Přidat novú'),
        'add_new_item'          => __( 'Přidat novú referenci'),
        'edit_item'             => __( 'Upravit referenci'),
        'new_item'              => __( 'Nová reference'),
        'view_item'             => __( 'Zobrazit referenci'),
        'search_items'          => __( 'Vyhledávat referenci'),
        'not_found'             => __( 'No item found'),
        'not_found_in_trash'    => __( 'No item found in Trash')
        );

    $args = array(
        'labels'                => $labels,
        'public'                => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'menu_icon'             => get_template_directory_uri() . '/includes/images/icon-testimonials.png',
        'rewrite' => array('slug'=>'reference'),
        'capability_type'       => 'page',
        'show_in_nav_menus'     => true,
        'supports'              => array('title','editor') 
        );
    register_post_type('hjtestimonials', $args);
    flush_rewrite_rules();
});


// room metaboxes
add_filter( 'cmb2_meta_boxes', 'hjtestimonials_metaboxes' );

function hjtestimonials_metaboxes( array $meta_boxes ) {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_testimonial_';

    /**
     * Sample metaboxes
     */

    $meta_boxes['testimonial_entry'] = array(
        'id'            => 'testimonial-entry',
        'title'         => __( 'Informace o klientovi'),
        'object_types'  => array( 'hjtestimonials', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
        'fields'        => array(
            array(
                'name' => __( 'Hodnocení'),
                'desc' => __( 'Ze seznamu vyberte hodnocení 1-5 hvězd.'),
                'id'   => $prefix . 'client_rating',
                'type'    => 'select',
                'show_options_none' => 'Zvolte hodnocení',
                'default' => '5star',
                'options'          => array(
                    'stars1' => __( '1 hvězda' ),
                    'stars2'   => __( '2 hvězdy' ),
                    'stars3'     => __( '3 hvězdy' ),
                    'stars4' => __( '4 hvězdy' ),
                    'stars5'   => __( '5 hvězdy' )
                ),
            )
        ),
    );

    
    return $meta_boxes;
}
