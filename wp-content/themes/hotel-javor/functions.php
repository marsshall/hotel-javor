<?php

/**
 * Metaboxes setup
 */
// Include and setup custom metaboxes and fields.
if ( file_exists( dirname( __FILE__ ) . '/includes/metaboxes/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/includes/metaboxes/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/includes/metaboxes/init.php' ) ) {
    require_once dirname( __FILE__ ) . '/includes/metaboxes/init.php';
}

/**
 * Maintenance mode
 */
require_once( TEMPLATEPATH . '/includes/maintenance.php');

/**
 *Theme specifics
 */

 // Include theme init
require_once( TEMPLATEPATH . '/includes/theme-init.php' );

 // Include sidebar init
require_once( TEMPLATEPATH . '/includes/sidebar-init.php' );

// Include breadcrumbs
require_once( TEMPLATEPATH . '/includes/breadcrumb.php' );

// Include gallery
require_once( TEMPLATEPATH . '/includes/gallery.php' );

// Include Tiny MCE custom styles
require_once( TEMPLATEPATH . '/includes/tinymce-styles.php' );


/**
 *Custom post types
 */

// Include partners
require_once( TEMPLATEPATH . '/includes/cpt/partners.php' );

// Include rooms
require_once( TEMPLATEPATH . '/includes/cpt/rooms.php' );

// Include slider
require_once( TEMPLATEPATH . '/includes/cpt/slider.php' );

// Include testimonials
require_once( TEMPLATEPATH . '/includes/cpt/testimonials.php' );

// Include plugin activation init
require_once( TEMPLATEPATH . '/includes/plugin-activation-init.php' );



