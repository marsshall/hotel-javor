<?php
/**
* Template Name: Reference klientů
*/
get_header(); ?>

<main role="main">
    <?php if ( has_post_thumbnail() ) { 
      $has_thumb = 'has-thumbnail';
      } 
      else {
        $has_thumb = '';
      }
    ?>
    <section class="page-head <?php echo $has_thumb; ?>">
         <?php if ( has_post_thumbnail() ) { 
          $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
          ?>
           <div class="ph-title">
             <div class="inn" style="background: url('<?php echo $thumbnail_src[0]; ?>') no-repeat center;background-size:cover;">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                       <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
             </div>
           </div>
          <?php } else { ?>
            <div class="ph-title">
               <div class="inn">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                     <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
               </div>
            </div>
          <?php } ?>
          <div class="ph-breadcrumbs">
           <div class="inn">
              <div class="container">
                <?php the_breadcrumb(); ?>
              </div>
           </div>
          </div>
    </section>
    <section class="page-content">
      <div class="container">
         <div class="content">
           <div class="testimonials">
             <div class="page-row">
               <div class="page-half">
                 <?php
                  // Post type args
                  $post_type = 'hjtestimonials';
                  $args=array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'caller_get_posts'=> 1,
                    'order_by' => 'rand'
                  );

                  // The Query
                  $testimonials_query = null;
                  $testimonials_query = new WP_Query($args); 
                ?>

                <?php if ( $testimonials_query->have_posts() ) : ?>
                 <h2 class="headline-ico headline-ico-quoting">Napsali o nás klienti</h2>
                 <div class="entry">
                   <div class="testimonials-list">
                     <?php 
                        while ( $testimonials_query->have_posts() ) : $testimonials_query->the_post();
                        $client_rating = get_post_meta( get_the_ID(), '_testimonial_client_rating', true );
                      ?>
                        <div class="testimonial-entry">
                          <div class="testimonial-text">
                            <?php the_content(); ?>
                          </div>
                          <div class="testimonial-meta">
                            <span class="client-name">
                              <?php the_title(); ?>
                            </span>
                            <?php if( ! empty( $client_rating ) ) :?>
                                <span class="client-rating <?php echo $client_rating; ?>"></span>
                              <?php endif; ?>
                          </div>
                        </div>
                      <?php endwhile; ?>
                   </div>
                 </div>
                 <?php endif; wp_reset_postdata(); ?>
               </div>
               <div class="page-half">
                 <h2 class="headline-ico headline-ico-star">Vyjádřete svůj názor na naše služby</h2>
                 <div class="entry">
                   <?php echo do_shortcode('[contact-form-7 id="346" title="Reference klientů"]'); ?>
                 </div>
               </div>
             </div>
           </div>
         </div>

         <?php dynamic_sidebar("socialize"); ?>
      </div>
    </section>
</main>


<?php get_footer(); ?>