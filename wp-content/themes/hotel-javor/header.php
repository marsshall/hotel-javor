<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php the_title(); ?></title>
  <!--[if lt IE 9]>
  <script src="<?php echo get_template_directory_uri() ?>/assets/js/html5shiv.js"></script>
  <script src="<?php echo get_template_directory_uri() ?>/assets/js/respond.min.js"></script>
  <![endif]-->       
<?php wp_head(); ?>
</head>

<body <?php body_class() ?>>

  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/sk_SK/sdk.js#xfbml=1&version=v2.3";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <div class="fb-slide-out">
      <a class="handle" href="http://link-for-non-js-users.html">fb</a>
      <div class="fb-page" data-href="https://www.facebook.com/pages/Hotel-Javor/148062288581627" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/pages/Hotel-Javor/148062288581627"><a href="https://www.facebook.com/pages/Hotel-Javor/148062288581627">Hotel Javor</a></blockquote></div></div>
  </div>

  <?php if( current_user_can('editor') || current_user_can('administrator') ) { ?>
  <div class="admin-bar">
      <a title="Administrace" class="admin-link" href="<?php echo bloginfo("siteurl") ?>/wp-admin/" target="_blank"> <i class="icon icon-gear"></i> <span>Administrace</span> </a> 
  </div>
  <?php } ?>

  <header role="banner">
    <div class="stripes">
      <div></div>
      <div></div>
    </div>
    <div class="container">
      <div class="brand">
          <a class="logo" href="<?php bloginfo('url')?>">
            <img src="<?php bloginfo('url')?>/wp-content/themes/hotel-javor/assets/images/logo-trans.png" alt="Horský hotel Javor">
          </a>
          <!-- <div class="location">
            <span>Dolní malá Úpa</span>
            <span>Krkonoše</span>
          </div> -->
          <button type="button" data-toggle="collapse" data-target="#header-navigation" class="collapsed"></button>
        </div>
        <div id="sticky-nav" class="navigation">
          <div id="header-navigation" class="collapse" style="height: 0px;">
            <a href="<?php bloginfo('url')?>" class="sticky-title"><?php bloginfo('name')?></a>
            <?php 
            if ( has_nav_menu( 'header-menu' ) ) {
              wp_nav_menu( array(
                'theme_location'  => 'header-menu',
                'container'       => false,
                'menu_class'      => '',
                'menu_id'         => '',
                'fallback_cb'     => ''
                )
              ); 
            }
            ?>
          </div>
        </div>
        <a target="_blank" href="http://www.babyfriendlycertificate.cz/instituce/horsky-hotel-javor-865.html" class="certificate-top"></a>
    </div>
  </header>






