<?php get_header(); ?>

<main role="main">
    <section class="page-head">
       <div class="ph-title">
           <div class="inn">
             <div class="container">
               <h1><?php $category = get_the_category(); echo $category[0]->cat_name; ?></h1>
               <div class="ph-contacts">
                 <?php dynamic_sidebar("reception-contacts"); ?>
               </div>
             </div>
           </div>
        </div>
        <div class="ph-breadcrumbs">
         <div class="inn">
            <div class="container">
              <?php the_breadcrumb(); ?>
            </div>
         </div>
        </div>
    </section>
    <section class="page-content">
        <div class="container">
             <div class="page-row">
               <div class="content">
                 <?php if (have_posts()) : ?> 
                 <div class="articles archive">
                   <?php while (have_posts()) : the_post(); ?>
                   <article>
                      <div class="article-date">
                        <span class="day"><?php the_time('d'); ?></span>
                        <span class="month"><?php the_time('M'); ?></span>
                      </div>
                      <?php if ( has_post_thumbnail() ) : ?> 
                      <div class="article-image">
                        <a href="<?php echo get_permalink(); ?>">
                          <?php the_post_thumbnail( 'thumbnail' ); ?>
                        </a>
                      </div>
                      <?php endif; ?>
                      <div class="article-entry">
                        <h3>
                            <a href="<?php echo get_permalink(); ?>">
                               <?php the_title(); ?>
                            </a>
                        </h3>
                          <?php the_content(); ?>
                      </div>
                    </article>
                 <?php endwhile ?> 
                 <?php else :?> 
                    <div class="entry">
                      Litujeme, ale v této sekci se prozatím nenacházejí žádné příspěvky 
                    </div> 
                 </div>
                <?php endif; ?>
               </div>
                <?php dynamic_sidebar("socialize"); ?>
           </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>