<?php get_header(); ?>
<main role="main">
    <?php if ( has_post_thumbnail() ) { 
      $has_thumb = 'has-thumbnail';
      } 
      else {
        $has_thumb = '';
      }
    ?>
    <section class="page-head <?php echo $has_thumb; ?>">
         <?php if ( has_post_thumbnail() ) { 
          $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
          ?>
           <div class="ph-title">
             <div class="inn" style="background: url('<?php echo $thumbnail_src[0]; ?>') no-repeat center;background-size:cover;">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                       <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
             </div>
           </div>
          <?php } else { ?>
            <div class="ph-title">
               <div class="inn">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                     <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
               </div>
            </div>
          <?php } ?>
          <div class="ph-breadcrumbs">
           <div class="inn">
              <div class="container">
                <?php the_breadcrumb(); ?>
              </div>
           </div>
          </div>
    </section>
    <section class="page-content">
        <div class="container">
          <div class="content">
             <div class="room-overview">  
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php  
                  // Grab the metadata from the database
                  $arrival_info_id = get_post_meta( get_the_ID(), '_room_arrival_info', true );
                  $general_info = get_post_meta( get_the_ID(), '_room_accommodation_info', true );
                  $room_description = get_post_meta( get_the_ID(), '_room_description', true );
                  $room_capacity = get_post_meta( get_the_ID(), '_room_capacity', true );
                  $room_price = get_post_meta( get_the_ID(), '_room_price', true );
                  $price_halfboard = get_post_meta( get_the_ID(), '_room_price_halfboard', true );
                  $price_children = get_post_meta( get_the_ID(), '_room_price_children', true );
                  $room_accessories_id = get_post_meta( get_the_ID(), '_room_accessories', true );
                  $services_id = get_post_meta( get_the_ID(), '_room_services', true );
                  $photogallery = get_post_meta( get_the_ID(), '_room_photogallery', true );
                ?>
                
                  <div class="page-row">
                    <div class="page-half">
                      <!-- General info -->
                      <div class="general-info">
                        <h2 class="headline-ico headline-ico-info">Informace</h2>
                        <div class="entry">
                          <?php echo apply_filters('the_content', $general_info); ?>
                        </div>
                        <div class="entry">
                          <h3><?php echo get_post_field('post_title', $arrival_info_id); ?></h3>
                          <?php echo apply_filters('the_content', get_post_field('post_content', $arrival_info_id)); ?>
                        </div>
                      </div><!-- //end General info -->
                    </div>
                    <div class="page-half">
                      <!-- Price list -->
                      <div class="price-list">
                        <h2 class="headline-ico headline-ico-price">Cenník</h2>
                        <div class="entry">
                          <?php echo apply_filters('the_content', $room_description); ?>
                          <table class="hj-table">
                            <thead>
                              <tr>
                                <th>
                                  Typ pokoje
                                </th>
                                <th class="text-center">
                                  Kapacita
                                </th>
                                <th class="text-center">
                                  Cena za pokoj se snídaní
                                </th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                              <td>
                                <?php the_title(); ?>
                              </td>
                              <td class="text-center">
                                <span class="capacity"><?php echo $room_capacity;?></span>
                              </td>
                              <td class="text-center">
                                <span class="price"><?php echo $room_price;?></span>
                              </td>
                              </tr>
                            </tbody>
                          </table>
                          <?php if( ! empty( $price_halfboard ) ) :?>
                          <p class="other-price">
                            <span>* Cena za pokoj s polopenzí:</span>
                            <span class="price"><?php echo $price_halfboard;?></span>
                          </p>
                          <?php endif; ?>
                          <?php if( ! empty( $price_children ) ) :?>
                          <p class="other-price">
                            <span>** Cena za dítě 4 - 9,99 let za noc se snídaní:</span>
                            <span class="price"><?php echo $price_children;?></span>
                          </p>
                          <?php endif; ?>

                          <?php
                            // Post type args
                            $post_type = 'hjrooms';
                            $args=array(
                              'post_type' => $post_type,
                              'post_status' => 'publish',
                              'posts_per_page' => -1,
                              'caller_get_posts'=> 1,
                              'post__not_in'=> array($post->ID)
                            );

                            // The Query
                            $the_query = null;
                            $the_query = new WP_Query($args); 
                          ?>
                          <?php if ( $the_query->have_posts() ) : ?>
                          <div class="other-rooms">
                            <h3>Na výběr jsou také tyto pokoje</h3>
                            <ul>
                              <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                              <li><a href="<?php echo post_permalink(); ?>"><?php the_title();?></a></li>
                              <?php endwhile; ?>
                            </ul>
                          </div>
                          <?php endif; wp_reset_postdata(); ?>
                        </div>
                      </div><!--//end Price list -->
                    </div>
                  </div>

                  <div class="page-row">
                    <div class="page-half">
                      <!-- Room accessories -->
                      <div class="room-accessories">
                        <h2 class="headline-ico headline-ico-hanger"><?php echo apply_filters('the_content', get_post_field('post_title', $room_accessories_id)); ?></h2>
                        <div class="entry">
                          <?php echo apply_filters('the_content', get_post_field('post_content', $room_accessories_id)); ?>
                        </div>
                      </div><!--//end Room accessories -->
                    </div>
                    <div class="page-half">
                      <!-- Services -->
                      <div class="services">
                        <h2 class="headline-ico headline-ico-bell"><?php echo apply_filters('the_content', get_post_field('post_title', $services_id)); ?></h2>
                        <div class="entry">
                          <?php echo apply_filters('the_content', get_post_field('post_content', $services_id)); ?>
                        </div>
                      </div><!--//end Services -->
                    </div>
                  </div>
                  
                  <?php if( ! empty( $photogallery ) ) :?>
                  <div class="page-row">
                    <div class="page-full">
                      <!-- Room Gallery -->
                      <div class="room-gallery">
                        <h2 class="headline-ico headline-ico-camera">Fotogalerie</h2>
                          <div class="entry animated-when-visible" data-animation-type="fadeInUp">
                            <?php echo apply_filters('the_content', $photogallery); ?>
                          </div>
                      </div> <!-- Room Gallery -->
                    </div>
                  </div>
                  <?php endif; ?>

                <?php endwhile; endif ?>
             </div>  
          </div>

          <?php dynamic_sidebar("socialize"); ?>
        </div>
    </section>
</main>


<?php get_footer(); ?>