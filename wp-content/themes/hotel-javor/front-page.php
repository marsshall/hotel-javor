<?php get_header(); ?>

<div id="main-slider" class="carousel slide carousel-fade" data-ride="carousel"> 
    <?php
        $post_type = 'hjslider';
        $args=array(
          'post_type' => $post_type,
          'post_status' => 'publish',
          'posts_per_page' => -1,
          'caller_get_posts'=> 1
        );

        $slides = get_posts( $args );
        $total_slides = count($slides);
    ?>
   <!-- Wrapper for slides -->
   <div class="carousel-inner" role="listbox">
     <?php foreach ($slides as $key => $slider) { 
         // Grab the metadata from the database
         $button_url = get_post_meta( $slider->ID, '_slider_button_url', true );

         $button_text = get_post_meta( $slider->ID, '_slider_button_text', true );
     ?>
     <?php
         $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($slider->ID), 'full' );
         $url = $thumb['0'];
     ?>
      <div class="item <?php echo ($key==0) ? 'active' : '' ?>" style="background-image:url(<?=$url?>);">
         <div class="container">
             <div class="caption">
                <h1><?php echo get_the_title($slider->ID); ?></h1>
                <?php if( ! empty( $button_url ) ){ ?>
                     <a class="btn btn-lg btn-secondary btn-chevron-right" href="<?php echo $button_url; ?>"><?php echo $button_text; ?></a>
                <?php }?> 
             </div>
         </div>
      </div>
     <?php } // endforeach ?>
   </div>
   <!-- Controls -->
   <a class="left carousel-control" href="#main-slider" role="button" data-slide="prev">
      <span class="sr-only">Předchozí</span>
   </a>
   <a class="right carousel-control" href="#main-slider" role="button" data-slide="next">
      <span class="sr-only">Další</span>
   </a>
   <div class="carousel-overlay">
      <div class="container">
         <?php dynamic_sidebar("reception-contacts"); ?>

          <?php
            $args = array(
                'post_type' => 'post',
                'posts_per_page' => -1,
                'orderby' => 'date',
                'meta_query' => array(
                  array(
                      'key' => '_pts_featured_post'
                  )
              )
            );
            $featured_news_query = null;
            $featured_news_query = new WP_Query($args); 

            $countposts = $featured_news_query->post_count;
            $is_slideshow = '';

            if ($countposts >= '2') {
                $is_slideshow = 'slideshow';
            }
         ?>
         <?php if( $featured_news_query->have_posts() ) { ?>
         <div class="featured-news <?php echo $is_slideshow; ?>">
          <div class="fn-inn">
          <?php while ($featured_news_query->have_posts()) : $featured_news_query->the_post(); ?>
            <div class="fn-item">
              <a href="<?php echo get_permalink(); ?>">
                <div class="fn-image">
                  <?php the_post_thumbnail('full'); ?>
                </div>
                <div class="fn-title">
                  <span>Nové:</span>
                  <h4><?php the_title(); ?></h4>
                </div>
              </a>
            </div>
          <?php endwhile; ?>
          </div>
         </div>
       <?php } wp_reset_query(); ?>
      </div>
   </div>
</div>
<main role="main">
    <section class="page-head has-featured-nav">
       <div class="ph-inn">
         <div class="container">
           <?php 
              if ( has_nav_menu( 'featured-menu' ) ) {
                wp_nav_menu( array(
                  'theme_location'  => 'featured-menu',
                  'container'       => false,
                  'menu_class'      => 'feature-nav',
                  'menu_id'         => '',
                  'fallback_cb'     => '',
                  'walker'          => new Description_Walker
                  )
                ); 
              }
           ?>
         </div>
       </div>
    </section>
    <section class="page-content">
        <div class="container">
             <div class="content">
               <div class="page-row">
                   <div class="page-half">
                    <?php if (have_posts()) : while (have_posts()) : the_post();?>
                       <div class="home-intro">
                          <?php the_content(); ?>

                          <?php
                            // Post type args
                            $post_type = 'hjrooms';
                            $args=array(
                              'post_type' => $post_type,
                              'post_status' => 'publish',
                              'posts_per_page' => -1,
                              'caller_get_posts'=> 1,
                              'post__not_in'=> array($post->ID)
                            );

                            // The Query
                            $the_query = null;
                            $the_query = new WP_Query($args); 
                          ?>
                          <?php if ( $the_query->have_posts() ) : ?>
                          <div class="entry">
                            <h2>Možnosti ubytování</h2>
                            <div class="price-list">
                              <a href="<?php bloginfo('url')?>/ubytovani/" class="btn btn-chevron-right">
                                Více
                              </a>
                              <table class="hj-table">
                                <thead>
                                  <tr>
                                    <th>
                                      Typ pokoje
                                    </th>
                                    <th class="text-center">
                                      Kapacita
                                    </th>
                                    <th class="text-center">
                                      Cena za pokoj se snídaní
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    while ( $the_query->have_posts() ) : $the_query->the_post();
                                    $room_capacity = get_post_meta( get_the_ID(), '_room_capacity', true );
                                    $room_price = get_post_meta( get_the_ID(), '_room_price', true ); 
                                  ?>
                                  <tr>
                                  <td>
                                    <a href="<?php echo get_permalink() ?>">
                                      <?php the_title(); ?>
                                    </a>
                                  </td>
                                  <td class="text-center">
                                    <span class="capacity"><?php echo $room_capacity;?></span>
                                  </td>
                                  <td class="text-center">
                                    <span class="price"><?php echo $room_price;?></span>
                                  </td>
                                  </tr>
                                  <?php endwhile; ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <?php endif; wp_reset_postdata(); ?>
                       </div>
                    <?php endwhile; endif;  ?>
                 </div>
                 <div class="page-half">
                    <div class="articles">
                       <?php
                           $cat_id = '4';
                           $cat_name = get_cat_name( $cat_id );
                           $cat_link = get_category_link( $cat_id );
                           $args = array(
                               'post_type' => 'post',
                               'posts_per_page' => 5,
                               'orderby' => 'date',
                               'cat' => $cat_id
                           );
                           $articles_query = null;
                           $articles_query = new WP_Query($args);
                       ?>
                       <?php if( $articles_query->have_posts() ) { ?>
                       <a href="<?php echo esc_url( $cat_link ); ?>" class="btn btn-chevron-right">Více</a>
                       <h2><?php echo $cat_name; ?></h2>
                       <?php while ($articles_query->have_posts()) : $articles_query->the_post(); ?>
                          <article>
                            <div class="article-date">
                              <span class="day"><?php the_time('d'); ?></span>
                              <span class="month"><?php the_time('M'); ?></span>
                            </div>
                            <div class="article-entry">
                              <h3>
                                  <a href="<?php echo get_permalink(); ?>">
                                     <?php the_title(); ?>
                                  </a>
                              </h3>
                                <?php the_content(); ?>
                            </div>
                          </article>  
                       <?php
                           endwhile; 
                          }
                          wp_reset_query();
                       ?>
                    </div>
                 </div>
               </div>
               <div class="page-row">
                 <div class="page-half">
                   <div class="video-stream">
                     <?php dynamic_sidebar("video-stream"); ?>
                   </div>
                 </div>
                 <?php
                  // Post type args
                  $post_type = 'hjtestimonials';
                  $args=array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    'posts_per_page' => 3,
                    'caller_get_posts'=> 1,
                    'order_by' => 'rand'
                  );

                  // The Query
                  $testimonials_query = null;
                  $testimonials_query = new WP_Query($args); 
                ?>
                <?php if ( $testimonials_query->have_posts() ) : ?>
                 <div class="page-half">
                   <div class="testimonials-list"> 
                      <h2>Napsali o nás klienti</h2>
                      <a href="<?php echo get_bloginfo('url'); ?>/reference-klientu" class="btn btn-chevron-right">Více</a>
                      <?php 
                        while ( $testimonials_query->have_posts() ) : $testimonials_query->the_post();
                        $client_rating = get_post_meta( get_the_ID(), '_testimonial_client_rating', true );
                      ?>
                        <div class="testimonial-entry">
                          <div class="testimonial-text">
                            <?php the_content(); ?>
                          </div>
                          <div class="testimonial-meta">
                            <span class="client-name">
                              <?php the_title(); ?>
                            </span>
                            <?php if( ! empty( $client_rating ) ) :?>
                                <span class="client-rating <?php echo $client_rating; ?>"></span>
                              <?php endif; ?>
                          </div>
                        </div>
                      <?php endwhile; ?>
                   </div>
                 </div>
                <?php endif; wp_reset_postdata(); ?>
               </div>
             </div>
             <?php dynamic_sidebar("socialize"); ?>
        </div>
    </section>
</main>

<?php get_footer(); ?>
