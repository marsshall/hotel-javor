jQuery(function($) {
 

  $(document).ready(function () {

      // To the top
      var $root = $('html, body');
      $('.to-top').click(function() {
          $root.animate({
              scrollTop: $( $.attr(this, 'href') ).offset().top
          }, 500);
          return false;
      });

      // Featured news fadeIn slideshow
      var timer;
      $(".featured-news.slideshow .fn-item:gt(0)").hide();
      $(".featured-news.slideshow")
          // when mouse enters, clear the timer if it has been set
          .mouseenter(function() {
              if (timer) { clearInterval(timer) }
          })
          // when mouse goes out, start the slideshow
          .mouseleave(function() {
              timer = setInterval(function() {
                  $(".featured-news.slideshow .fn-item:first")
                      .fadeOut(1000)
                      .next()
                      .fadeIn(1000)
                      .end()
                      .appendTo(".featured-news.slideshow .fn-inn");
              }, 5000);
          })
          // trigger mouseleave for initial slideshow starting
          .mouseleave();

          // Show more handler
          $('.show-more .show-trigger').click(function() {
              $(this).toggleClass('open');
              $(this).parent('.show-more').find('.show-content').slideToggle();
          });

          
        // initialize Masonry
        var $container = $('.room-gallery .photogallery-inn').masonry();
        var $container = $('.articles.archive').masonry();

        // Sticky nav
        var sticky = $('#sticky-nav'),
        stickyTop = sticky.offset().top + 55,
        scrollTop,
        scrolled = false,
        $window = $(window);

        /* Bind the scroll Event */
        $window.on('scroll', function (e) {
           scrollTop = $window.scrollTop();

           if (scrollTop >= stickyTop ) {

               sticky.removeClass('scrolled-before').addClass('fixed');
           } else if (scrollTop < stickyTop) {

               sticky.addClass('scrolled-before').removeClass('fixed');
           }
        });

  });
  
  // Initialize Gallery script  
  $(document).ready(function(e) {
     $('a[data-lightbox-gallery]').iLightbox();
  });

  // Animated when visible
  $(".animated-when-visible").each(function(i, el) {
    el = $(el);
    if (el.visible(true)) {
      return el.addClass("animated " + el.data("animation-type"));
    }
  });
  $(window).scroll(function(event) {
    return $(".animated-when-visible").each(function(i, el) {
      el = $(el);
      if (el.visible(true)) {
        return el.addClass("animated " + el.data("animation-type"));
      }
    });
  });


  // TabSliderOut
  $(function(){
      $('.fb-slide-out').tabSlideOut({
          tabHandle: '.handle',                     //class of the element that will become your tab
          tabLocation: 'right',                      //side of screen where tab lives, top, right, bottom, or left
          speed: 300,                               //speed of animation
          action: 'click',                          //options: 'click' or 'hover', action to trigger animation
          topPos: '400px',                          //position from the top/ use if tabLocation is left or right
          leftPos: '20px',                          //position from left/ use if tabLocation is bottom or top
          fixedPosition: true                      //options: true makes it stick(fixed position) on scroll
      });
  });
  

});
