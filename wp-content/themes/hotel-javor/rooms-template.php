<?php
/**
* Template Name: Ubytování
*/
get_header(); ?>

<main role="main">
    <?php if ( has_post_thumbnail() ) { 
      $has_thumb = 'has-thumbnail';
      } 
      else {
        $has_thumb = '';
      }
    ?>
    <section class="page-head <?php echo $has_thumb; ?>">
         <?php if ( has_post_thumbnail() ) { 
          $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
          ?>
           <div class="ph-title">
             <div class="inn" style="background: url('<?php echo $thumbnail_src[0]; ?>') no-repeat center;background-size:cover;">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                       <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
             </div>
           </div>
          <?php } else { ?>
            <div class="ph-title">
               <div class="inn">
                 <div class="container">
                   <h1><?php the_title(); ?></h1>
                   <div class="ph-contacts">
                     <?php dynamic_sidebar("reception-contacts"); ?>
                   </div>
                 </div>
               </div>
            </div>
          <?php } ?>
          <div class="ph-breadcrumbs">
           <div class="inn">
              <div class="container">
                <?php the_breadcrumb(); ?>
              </div>
           </div>
          </div>
    </section>
    <section class="page-content">
        <div class="container">
             <div class="content">
               <h2>Typy pokojů</h2>
              <?php
                // Post type args
                $post_type = 'hjrooms';
                $args=array(
                  'post_type' => $post_type,
                  'post_status' => 'publish',
                  'posts_per_page' => -1,
                  'caller_get_posts'=> 1
                );

                // The Query
                $the_query = null;
                $the_query = new WP_Query($args); 
              ?>

              <?php if ( $the_query->have_posts() ) : ?>
               <ul class="rooms-list">
                  <?php while ( $the_query->have_posts() ) : $the_query->the_post();?>
                  <li>
                    <div class="room-item">
                      <?php if (class_exists('MultiPostThumbnails')) :
                        if ( MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'secondary-image')) :
                      ?>
                      <div class="ri-image">
                        <a href="<?php echo post_permalink(); ?>"><?php MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'secondary-image'); ?></a>
                      </div>
                      <?php endif; endif; ?>
                      <div class="ri-title">
                        <h3><?php the_title( );?></h3>
                        <a href="<?php echo post_permalink(); ?>" class="btn btn-chevron-right">více</a>
                      </div>
                  </li>
                <?php endwhile; ?>
               </ul>
               <?php endif; wp_reset_postdata(); ?>
             </div>

             <?php dynamic_sidebar("socialize"); ?>
        </div>
    </section>
</main>


<?php get_footer(); ?>