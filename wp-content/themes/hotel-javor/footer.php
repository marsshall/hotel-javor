<footer role="content-info">
   <div class="stripes">
      <div></div>
      <div></div>
   </div>

   <div class="footer-section">
      <div class="container">
         <div class="inn">
            <?php dynamic_sidebar("newsletter-subscribe"); ?>
            <div class="footer-nav">
               <?php dynamic_sidebar("footer-links"); ?>
            </div>
         </div>
      </div>
   </div>
   <?php
       // Post type args
       $post_type = 'hjpartners';
       $args=array(
         'post_type' => $post_type,
         'post_status' => 'publish',
         'posts_per_page' => -1,
         'caller_get_posts'=> 1
       );

       // The Query
       $the_query = null;
       $the_query = new WP_Query($args);

       if ( $the_query->have_posts() ) :
   ?>
   <div class="footer-section">
      <div class="container">
         <ul class="partners">    
         <?php // The Loop
             while ( $the_query->have_posts() ) : $the_query->the_post();
             // Grab the metadata from the database
             $partner_url = get_post_meta( get_the_ID(), '_partner_url', true );
         ?> 
          <li>
            <?php if( ! empty( $partner_url ) ){ ?>
                <a href="<?php echo $partner_url; ?>">
                    <?php echo get_the_post_thumbnail( get_the_ID(), 'full'); ?>
                </a>
            <?php } else { ?> 
                <?php echo get_the_post_thumbnail( get_the_ID(), 'full'); ?>
            <?php } ?>
          </li>
           <?php endwhile;  //end of loop
           // Reset Post Data
           wp_reset_postdata(); ?>
           </ul>
      </div>
   </div>
   <?php endif; ?>
   <div class="footer-copyright">
      <div class="container">
         <?php dynamic_sidebar("footer-copyright"); ?>
      </div>
   </div>
</footer>
<?php wp_footer(); ?>