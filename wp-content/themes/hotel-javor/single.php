<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<main role="main">
    <section class="page-head">
       <div class="ph-title">
           <div class="inn">
             <div class="container">
               <h1><?php $category = get_the_category(); echo $category[0]->cat_name; ?></h1>
               <div class="ph-contacts">
                 <?php dynamic_sidebar("reception-contacts"); ?>
               </div>
             </div>
           </div>
        </div>
        <div class="ph-breadcrumbs">
         <div class="inn">
            <div class="container">
              <?php the_breadcrumb(); ?>
            </div>
         </div>
        </div>
    </section>
    <section class="page-content">
        <div class="container">
           <div class="inn">
             <div class="sidebar">
               <ul class="side-nav">
                 <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
               </ul>
             </div>
             <div class="content has-sidebar">
               <article>
                 <h2><?php the_title();?></h2>
                 <div class="entry editor-output">
                   <?php the_content(); ?>
                 </div>
               </article>
             </div>
           </div>
        </div>
    </section>
</main>
<?php endwhile; endif; ?>

<?php get_footer(); ?>